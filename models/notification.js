var mongoose = require('mongoose');

var User = require("./user");

var NotificationSchema = new mongoose.Schema({
  content: {type: String, required: true},
  when: {type: Date, default: Date.now},
  what: String, // what type of notification
  for: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  unread: {type: Boolean, default:  true}

});

NotificationSchema.statics.addForFlat =  function (flatNo, block, content, cb) {
  var notificationModel = this;
  User
  .find({block: block, flatNo: flatNo, role: "Resident"})
  .exec(function (err, users) {
    if (err) {
      console.log(err);
      return cb(err);
    }
    var notifs = users.map(function (user) {
      return {
        content: content,
        what: 'flatnotification',
        for: user._id
      }
    })
    if (notifs.length === 0) {
      return cb(null, []); // to avoid `MongoError: Invalid Operation, No operations in bulk`
    }
    notificationModel.insertMany(notifs, function (err, docs) {
      if (err) {
        console.log(err);
        return cb(err);
      }
      return cb(null, docs);
    })
  })

}

NotificationSchema.statics.addForSociety =  function (societyId, content, cb) {
  var notificationModel = this;
  User
  .find({societyId: societyId, role: "Resident"})
  .exec(function (err, users) {
    if (err) {
      console.log(err);
      return cb(err);
    }
    var notifs = users.map(function (user) {
      return {
        content: content,
        what: 'societynotifications',
        for: user._id
      }
    })
    if (notifs.length === 0) {
      return cb(null, []); // to avoid `MongoError: Invalid Operation, No operations in bulk`
    }
    notificationModel.insertMany(notifs, function (err, docs) {
      if (err) {
        console.log(err);
        return cb(err);
      }
      return cb(null, docs);
    })
  })

}


NotificationSchema.set('toJSON', {virtuals: true});
NotificationSchema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Notification', NotificationSchema);
